Create IAM policy in AWS that allows our autoscaler to create the resources it needs

`aws iam create-policy --policy-name ig-policy --policy-document file://ig-policy.json`

Next, attach the newly created IAM policy to our role for our node cluster

`aws iam attach-role-policy --policy-arn arn:aws:iam::209925384246:policy/ig-policy --role-name nodes.demo.cloudchap.cf`

Next add the following cloudlabels to the kops instance group

```
spec:
  cloudLabels:
    k8s.io/cluster-autoscaler/enabled: ""
    k8s.io/cluster-autoscaler/node-template/label: ""
```
