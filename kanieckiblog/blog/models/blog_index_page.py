from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel

from .blog_page import BlogPage


class BlogIndexPage(Page):

    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [FieldPanel("intro", classname="full")]

    def get_context(self, request):
        context = super().get_context(request)

        # Get blog entries
        blog_entries = BlogPage.objects.child_of(self).live()

        # Filter by tag
        tag = request.GET.get("tag")
        if tag:
            blog_entries = blog_entries.filter(tags__name=tag)

        # category
        category = request.GET.get("category")
        if category:
            blog_entries = blog_entries.filter(categories__name=category)

        paginator = Paginator(blog_entries.order_by("-date"), 12)
        # Try to get the ?page=x value
        page = request.GET.get("page", 1)
        try:
            # If the page exists and the ?page=x is an int
            blog_entries = paginator.page(page)
        except PageNotAnInteger:
            # If the ?page=x is not an int; show the first page
            blog_entries = paginator.page(1)
            page = 1
        except EmptyPage:
            # If the ?page=x is out of range (too high most likely)
            # Then return the last page
            blog_entries = paginator.page(paginator.num_pages)
            page = paginator.num_pages

        context["blog_entries"] = blog_entries
        context["current_page"] = page
        context["max_page_index"] = paginator.num_pages

        return context
