from django.db import models

# Create your models here.


class ContactMessage(models.Model):

    name = models.CharField(max_length=200)

    email = models.EmailField()

    company = models.CharField(max_length=100, null=True, blank=True)

    website = models.URLField(null=True, blank=True)

    message = models.TextField()
