from django.contrib import admin

from .models import ContactMessage

# Register your models here.


@admin.register(ContactMessage)
class ContactMessageAdminConsole(admin.ModelAdmin):
    list_display = (
        "name",
        "email",
        "company",
        "website",
    )
