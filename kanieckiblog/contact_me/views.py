from django.shortcuts import render, redirect
from django.contrib import messages

from .models import ContactMessage
from .forms import ContactMeForm

# Create your views here.


def contact(request):
    if request.method == "POST":
        form = ContactMeForm(data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request, "Thank you for contacting me! I will get in touch shortly."
            )
    return render(request, "contact_me.html")
