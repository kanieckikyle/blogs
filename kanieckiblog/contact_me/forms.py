from django import forms
from .models import ContactMessage


class ContactMeForm(forms.ModelForm):
    class Meta:
        model = ContactMessage
        fields = ("name", "email", "company", "website", "message")

