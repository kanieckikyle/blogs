from django.views import View
from django.http import HttpResponse


class HealthCheckView(View):
    def get(self, request):
        return HttpResponse()
