import pyotp
from django import forms
from django.conf import settings
from django.contrib.admin import AdminSite
from django.contrib.admin.forms import AdminAuthenticationForm
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy


class TwoFactorAuthForm(AdminAuthenticationForm):

    otp = forms.CharField(required=False)

    def clean_otp(self):
        User = get_user_model()
        try:
            User.objects.get(username=self.cleaned_data.get("username"))
        except User.DoesNotExist:
            return
        code = self.cleaned_data.get("otp")
        totp = pyotp.TOTP(settings.TWO_FACTOR_KEY)
        if code != totp.now() and not settings.DEBUG:
            raise forms.ValidationError("OTP does not match")
        return self.cleaned_data


class KanieckiBlogAdminSite(AdminSite):
    # Text to put at the end of each page's <title>.
    site_title = ugettext_lazy("Kaniecki Blog Administartion")

    # Text to put in each page's <h1> (and above login form).
    site_header = ugettext_lazy("Kaniecki Blog administration")

    # Text to put at the top of the admin index page.
    index_title = ugettext_lazy("Kaniecki Blog administration")

    login_form = TwoFactorAuthForm

    login_template = "otp_admin_template.html"
