from django.db import models
from wagtail.core import blocks
from django.utils.translation import gettext_lazy as _
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import get_formatter_by_name
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string


class CodeBlock(blocks.StructBlock):
    class LANGUAGE_CHOICES(models.TextChoices):
        TEXT = "text", _("Text")
        BASH = "bash", _("Bash")
        CONSOLE = "console", _("Console Output")
        PYTHON = "python", _("Python")
        HTML = "html", _("HTML")
        CSS = "css", _("CSS")
        SCSS = "scss", _("SCSS")
        JAVASCRIPT = "javascript", _("Javascript")
        TYPESCRIPT = "typescript", _("Typescript")
        DOCKER = "docker", _("Docker")
        YAML = "yaml", _("YAML")
        INI = "ini", _("ini, cfg Files")

    language = blocks.ChoiceBlock(
        choices=LANGUAGE_CHOICES.choices, default=LANGUAGE_CHOICES.TEXT, max_length=15
    )

    code = blocks.TextBlock()

    caption = blocks.CharBlock(max_length=150, required=False, blank=True, null=True)

    class Meta:
        icon = "code"
        label = "Code"
        # template_name = "blocks/code_block.html"

    def render(self, value, *args, **kwargs):
        src = value["code"].strip("\n")
        caption = value["caption"].strip()
        lang = value["language"]
        lexer = get_lexer_by_name(lang)
        formatter = get_formatter_by_name("html", style="monokai", noclasses=True)
        render_content = highlight(src, lexer, formatter)

        template_html = render_to_string(
            "blocks/code_block.html",
            {"code": mark_safe(render_content), "caption": caption},
        )

        return template_html

