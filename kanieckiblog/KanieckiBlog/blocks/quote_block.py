from django.db import models
from wagtail.core import blocks
from django.utils.translation import gettext_lazy as _
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import get_formatter_by_name
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string


class QuoteBlock(blocks.StructBlock):

    quote = blocks.CharBlock()

    class Meta:
        icon = "placeholder"
        label = "Quote"
        template = "blocks/quote_block.html"

