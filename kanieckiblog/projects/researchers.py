from .models import Project
from urllib.parse import urlparse, urlencode
import requests


class BaseResearcher:
    """
    testing
    """

    sdk = None

    def research(self, project: Project) -> Project:
        """
        
        """
        raise NotImplementedError("This is an abstract method!")


class GitLabResearcher(BaseResearcher):

    sdk = None

    def research(self, project: Project) -> Project:
        url = urlparse(project.project_url)
        path = url.path.replace("/", "%2F")
        namespace_path = urlencode(path)

        return None


class GitHubResearcher(BaseResearcher):

    sdk = None

    def research(self, project: Project) -> Project:
        return None


def get_researcher_by_project_type(project_type: str) -> BaseResearcher:
    if project_type == "gitlab":
        return GitLabResearcher()
    if project_type == "github":
        return GitHubResearcher()
    return None

