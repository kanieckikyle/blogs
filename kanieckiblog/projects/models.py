from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.


class Project(models.Model):
    class ProjectTypeChoices(models.TextChoices):
        GITLAB = "gitlab", _("Gitlab")
        GITHUB = "github", _("Github")
        GENERAL = "general", _("General")

    project_type = models.CharField(
        max_length=15,
        choices=ProjectTypeChoices.choices,
        default=ProjectTypeChoices.GENERAL,
    )

    project_url = models.URLField()
