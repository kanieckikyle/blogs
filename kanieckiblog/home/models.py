from django.db import models

from wagtail.core.models import Page
from blog.models import BlogPage
from wagtail.snippets.models import register_snippet
from wagtail.admin.edit_handlers import FieldPanel


@register_snippet
class Skill(models.Model):
    name = models.CharField(max_length=30)

    rank = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name


class HomePage(Page):

    overview = models.TextField(default="")

    content_panels = Page.content_panels + [
        FieldPanel("overview"),
    ]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["kaniecki_skills"] = Skill.objects.all().order_by("rank")
        context["recent_blog_posts"] = BlogPage.objects.live().order_by("-date")[:4]

        return context
